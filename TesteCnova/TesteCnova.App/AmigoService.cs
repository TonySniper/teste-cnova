﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteCnova.App
{
    public class AmigoService
    {
        public IList<Amigo> InsertAmigos()
        {
            var amigos = new List<Amigo>();

            //return new List<Amigo>
            //{
            //    new Amigo { Nome = "Antonio", X = 10, Y = 30 },
            //    new Amigo { Nome = "Bárbara", X = -21, Y = 11 },
            //    new Amigo { Nome = "João", X = -46, Y = -20 },
            //    new Amigo { Nome = "Raphael", X = -1, Y = 1 },
            //    new Amigo { Nome = "Paulo", X = 8, Y = 12 },
            //    new Amigo { Nome = "Leonardo", X = -44, Y = 66 },
            //    new Amigo { Nome = "Antonidas", X = 49, Y = 55 },
            //    new Amigo { Nome = "Janaína", X = 21, Y = 30 },
            //    new Amigo { Nome = "Brodestone", X = 1, Y = 0 },
            //};

            Console.WriteLine("Para sair, digite \"sair\" a qualquer momento\n");
            try
            {
                while (true)
                {
                    Console.WriteLine("Digite o nome do amigo");
                    string nome = Console.ReadLine();

                    if (IsSair(nome))
                        return amigos;

                    Console.WriteLine("Digite a posição X do amigo");
                    string posX = Console.ReadLine();
                    if (IsSair(posX))
                        return amigos;

                    Console.WriteLine("Digite a posição Y do amigo");
                    string posY = Console.ReadLine();
                    if (IsSair(posY))
                        return amigos;

                    var amigo = new Amigo { Nome = nome, X = int.Parse(posX), Y = int.Parse(posY) };

                    if (amigos.Any(x => x.X == amigo.X && x.Y == amigo.Y))
                    {
                        Console.WriteLine("Amigo com mesma posição de amigo existente; ignorando amigo");
                        continue;
                    }

                    amigos.Add(amigo);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Amigo GetNearestAmigo(List<Amigo> listaAmigos, Amigo amigoAtual)
        {
            return listaAmigos.Where(x => x.X != amigoAtual.X && x.Y != amigoAtual.Y)
                .OrderBy(x => Math.Pow(amigoAtual.X - x.X, 2) + Math.Pow(amigoAtual.Y - x.Y, 2)).First();
        }

        private bool IsSair(string cmd)
        {
            return string.Compare(cmd.ToLower(), "sair") == 0;
        }
    }
}
