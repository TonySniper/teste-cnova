﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteCnova.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var svc = new AmigoService();

            var amigos = svc.InsertAmigos();

            foreach (var item in amigos)
            {
                Console.WriteLine("Amigos próximos de: {0}; x = {1}; y = {2}", item.Nome, item.X, item.Y);
                var listaAmigos = amigos.ToList();

                for(int i = 0; i < 3; i++)
                {
                    var nearestAmigo = svc.GetNearestAmigo(listaAmigos, item);
                    Console.WriteLine("{0}; x = {1}; y = {2}", nearestAmigo.Nome, nearestAmigo.X, nearestAmigo.Y);
                    listaAmigos.Remove(nearestAmigo);
                }
            }
        }
    }
}
