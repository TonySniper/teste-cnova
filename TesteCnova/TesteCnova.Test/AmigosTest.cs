﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TesteCnova.App;
using System.Linq;

namespace TesteCnova.Test
{
    [TestClass]
    public class AmigosTest
    {
        List<Amigo> amigos;
        AmigoService svc = new AmigoService();

        [TestInitialize]
        public void SetUp()
        {
            this.amigos = new List<Amigo>
            {
                new Amigo { Nome = "Antonio", X = 10, Y = 30 },
                new Amigo { Nome = "Bárbara", X = -21, Y = 11 },
                new Amigo { Nome = "João", X = -46, Y = -20 },
                new Amigo { Nome = "Raphael", X = -1, Y = 1 },
                new Amigo { Nome = "Paulo", X = 8, Y = 12 },
                new Amigo { Nome = "Leonardo", X = -44, Y = 66 },
                new Amigo { Nome = "Antonidas", X = 49, Y = 55 },
                new Amigo { Nome = "Janaína", X = 21, Y = 30 },
                new Amigo { Nome = "Brodestone", X = 1, Y = 0 },
            };

            //var amigo = new Amigo { Nome = "Brodestone", X = 1, Y = 0 };
        }

        [TestCleanup]
        public void CleanUp()
        {
            this.amigos = new List<Amigo>
            {
                new Amigo { Nome = "Antonio", X = 10, Y = 30 },
                new Amigo { Nome = "Bárbara", X = -21, Y = 11 },
                new Amigo { Nome = "João", X = -46, Y = -20 },
                new Amigo { Nome = "Raphael", X = -1, Y = 1 },
                new Amigo { Nome = "Paulo", X = 8, Y = 12 },
                new Amigo { Nome = "Leonardo", X = -44, Y = 66 },
                new Amigo { Nome = "Antonidas", X = 49, Y = 55 },
                new Amigo { Nome = "Janaína", X = 21, Y = 30 },
                new Amigo { Nome = "Brodestone", X = 1, Y = 0 },
            };
        }

        [TestMethod]
        public void Deve_Trazer_Um_Amigo_Proximo_Correto()
        {
            var origin = new Amigo { Nome = "origin", X = 99, Y = -99 };
            var target = new Amigo { Nome = "target", X = 88, Y = -88 };

            this.amigos.Add(origin);
            this.amigos.Add(target);

            var result = this.svc.GetNearestAmigo(this.amigos, origin);

            Assert.AreEqual(target.Nome, result.Nome);
        }

        [TestMethod]
        public void Deve_Trazer_Tres_Amigos_Proximos_Corretos()
        {
            var origin = new Amigo { Nome = "origin", X = -33, Y = 29 };
            var amigo1 = new Amigo { Nome = "amigo1", X = -35, Y = 24 };
            var amigo2 = new Amigo { Nome = "amigo2", X = -28, Y = 32 };
            var amigo3 = new Amigo { Nome = "amigo3", X = -31, Y = 31 };

            this.amigos.Add(origin);
            this.amigos.Add(amigo1);
            this.amigos.Add(amigo2);
            this.amigos.Add(amigo3);

            var listaRetorno = new List<Amigo>();
            for (int i = 0; i < 3; i++)
            {
                var result = this.svc.GetNearestAmigo(this.amigos, origin);
                this.amigos.Remove(result);
                listaRetorno.Add(result);
            }

            Assert.IsTrue(listaRetorno.Any(x => string.Compare(x.Nome, amigo1.Nome) == 0));
            Assert.IsTrue(listaRetorno.Any(x => string.Compare(x.Nome, amigo2.Nome) == 0));
            Assert.IsTrue(listaRetorno.Any(x => string.Compare(x.Nome, amigo3.Nome) == 0));
        }
    }
}
